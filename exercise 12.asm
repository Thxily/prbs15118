define aRandom      $fe
define aStartScreen $0200

define vZero        $00

  ldx #vZero

loop:
  lda aRandom
  sta aStartScreen,x
  lda aRandom
  sta $0300,x
  lda aRandom
  sta $0400,x
  lda aRandom
  sta $0500,x
  inx
  bne loop